﻿using EscuelaNet.Dominio.SeedWoork;

namespace EscuelaNet.Dominio.Proyectos
{
    public class LineaDeProduccion : Entity
    {
        public string Nombre { get; set; }
    }
}