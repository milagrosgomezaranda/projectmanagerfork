﻿namespace EscuelaNet.Dominio.Proyectos
{
    public class Etapa
    {
        public string Nombre { get; set; }
        public int Duracion { get; set; }
        private Etapa()
        {
            this.Duracion = 0;
        }
        public Etapa(string nombre)
        {
            this.Nombre = nombre ?? throw new System.ArgumentNullException(nameof(nombre));
        }
        public void CambiarDuracion(int duracion)
        {
            if (duracion <= 0)
                throw new ExcepcionDeProyectos("Una etapa debe tener más de 0 días.");
            this.Duracion = duracion;
        }
    }
}